from influxdb import InfluxDBClient
import dash
import dash_core_components as dcc
import dash_html_components as html
import json


client = InfluxDBClient(host='localhost',port=8086)
client.switch_database('coba')
#
res = client.query("select * from tes")
rows = res.get_points(tags={'alat': 'A'})
adata = []
for row in rows:
     adata.append(row['value'])


app = dash.Dash()

app.layout = html.Div([
    dcc.Graph(id='my-graph',
       figure={
            'data': [
                {'y': adata},
            ],
            'layout': {
                'title': 'Dash Data Visualization'
            }
        }
  )
])
    
#jalankan di localhost:8010    
app.run_server(port=8010,debug=False)

"""
# DATA dari tabel tes
Time : 2018-11-19T08:01:54.590356633Z  Value : 0.570000 
Time : 2018-11-19T08:02:04.749823267Z  Value : 0.610000 
Time : 2018-11-19T08:02:07.990194623Z  Value : 0.630000 
Time : 2018-11-19T08:02:12.286846974Z  Value : 0.490000 
Time : 2018-11-30T08:40:04.795076146Z  Value : 1.220000 
Time : 2018-11-30T08:42:54.036248931Z  Value : 0.740000 
Time : 2018-11-30T08:56:41.287259452Z  Value : 0.740000 
Time : 2018-11-30T09:11:57.508862899Z  Value : 1.330000 
"""