from influxdb import InfluxDBClient
import json
import dash
from dash.dependencies import Output, Event
import dash_core_components as dcc
import dash_html_components as html
import plotly
import plotly.graph_objs as go


client = InfluxDBClient(host='localhost',port=8086)
client.switch_database('coba')


app = dash.Dash()

app.layout = html.Div([
	dcc.Graph(id='live-graph',animate=True),
	dcc.Interval(
		id='graph-update',
		interval=2*1000
	)

])
    

@app.callback(Output('live-graph','figure'),
		events=[Event('graph-update','interval')])
def update_graph():

	res = client.query("select * from tes order by time desc limit 20")
	rows = res.get_points(tags={'alat': 'A'})
	adata = []
	atime = []
	for row in rows:
	     adata.append(row['value'])
	     atime.append(row['time'])

	data = go.Scatter(
#		x = atime,
		y = adata,
		name = 'Scatter',
		mode = 'lines+markers'
	)
	return {'data':[data], 'layout':{'title':'Data Influx'}}


  
app.run_server(port=8010,debug=False)
